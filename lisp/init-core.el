;; package management

(require 'package)

(setq package-archives '(("org"       . "http://orgmode.org/elpa/")
                         ("gnu"       . "http://elpa.gnu.org/packages/")
                         ("melpa"     . "https://melpa.org/packages/"))
												 use-package-always-ensure t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(setq use-package-compute-statistics t)

;; sane defaults
(use-package emacs
  :init
  (setq delete-old-versions -1 )		; delete excess backup versions silently
  (setq version-control t )					; use version control
  (setq vc-make-backup-files t )		; make backups file even when in version controlled dir
  (setq backup-directory-alist `(("." . "~/.emacs.d/backups")) ) ; which directory to put backups file
  (setq vc-follow-symlinks t )				       ; don't ask for confirmation when opening symlinked file
  (setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)) ) ; transform backups file name
  (setq inhibit-startup-screen t )	; inhibit useless and old-school startup screen
  (setq ring-bell-function 'ignore )	; silent bell when you make a mistake
  (setq coding-system-for-read 'utf-8 )	; use utf-8 by default
  (setq coding-system-for-write 'utf-8 )
  (setq sentence-end-double-space nil)	; sentence SHOULD end with only a point.
  (setq default-fill-column 120)				; toggle wrapping text at the 80th character
  (setq show-trailing-whitespace t)
  (defalias 'yes-or-no-p 'y-or-n-p)			; life is too short
  (setq-default indent-tabs-mode nil
                tab-width 2)
  (recentf-mode t)

  (setq vc-follow-symlinks t)
  ;; clean up the mode line
  (display-time-mode -1)
  (setq column-number-mode t)

  (set-frame-position nil 0 -24)
  (add-to-list 'default-frame-alist '(fullscreen . maximized))
  )

;; garbage collection
(use-package gcmh
  :demand
  :config
  (gcmh-mode 1))


;; keybindings
(use-package general
  :config
  (general-evil-setup)

  (general-create-definer cat/leader-keys
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC"
    :global-prefix "C-SPC")

  (general-create-definer cat/local-leader-keys
    :states '(normal visual)
    :keymaps 'override
    :prefix ","
    :global-prefix "SPC m")

  (cat/leader-keys
    "/"   'helm-rg
    "TAB" '(cat/switch-to-other-buffer :which-key "prev buffer")
    "SPC" 'helm-M-x
    "<escape>" 'keyboard-escape-quit

    ;; Buffers
    "b" '(:ignore t :which-key "Buffers")
    "bb" 'helm-mini
    "bd" 'kill-current-buffer

    ;; Emacs
    "e" '(:ignore t :which-key "Emacs")
    "ee" 'cat/edit-emacs-config
    "er" 'cat/reload-emacs-config

		;; Files
		"f" '(:ignore t :which-key "Files")
		"ff" 'helm-find-files
		"fs" 'save-buffer
    "fy" '(:ignore t :which-key "Yank/Copy")
    "fy d" '(cat/copy-directory-path :wk "directory path")
    "fy n" '(cat/copy-file-name :wk "file name")
    "fy N" '(cat/copy-file-name-base :wk "file name without extension")
    "fy y" '(cat/copy-file-path :wk "file path")

    "g" '(:ignore t :which-key "git")

    "s" '(:ignore t :which-key "Search")
    "sf" 'helm-rg
    "sl" 'helm-resume

    "t"  '(:ignore t :which-key "toggle")
    "t d"  '(toggle-debug-on-error :which-key "debug on error")
    "t l" '(display-line-numbers-mode :wk "line numbers")
    "t w" '((lambda () (interactive) (toggle-truncate-lines)) :wk "word wrap")

    ;; Windows
		"w" '(:ignore t :which-key "Windows")
    "wd"  'delete-window
    "wl"  'windmove-right
    "wh"  'windmove-left
    "wk"  'windmove-up
    "wj"  'windmove-down
    "ws"  'split-window-below
    "wS"  'split-window-below-and-focus
    "wv"  'split-window-right
    "wV"  'split-window-right-and-focus

    ;; custom
    ;;";" '(:ignore t :which-key "Custom")
    ;;";;" 'evilnc-comment-operator
    ";"	'evilnc-comment-operator

    ;; Applications
    ;; "a" '(:ignore t :which-key "Applications")
    ;; "ar" 'ranger
    "ad" 'dired)
  :init
  (defun cat/edit-emacs-config ()
    (interactive)
    (find-file "~/.emacs.d/init.el"))

  (defun cat/reload-emacs-config ()
    (interactive)
    (load user-init-file))

  ;; print (window-prev-buffers) for debug
  (defun cat/switch-to-other-buffer ()
    "Switch back and forth between current and last buffer in the
current window, limited to buffers within the current perspective."
  (interactive)
  (let* ((buffer-list (persp-current-buffers)) ; Get list of buffers in the current perspective
         (current-buffer (current-buffer))
         (window (selected-window)) ; Operate on the current window
         (prev-buffers (window-prev-buffers window))
         (target-buffer
          (seq-find (lambda (buffer-info)
                      (let ((buffer (car buffer-info)))
                        (and (not (eq buffer current-buffer))
                             (member buffer buffer-list))))
                    prev-buffers)))
    ;; If a suitable buffer was found, switch to it
    (if target-buffer
        (set-window-buffer window (car target-buffer))
      (message "No suitable buffer found in the current perspective."))))

  (defun cat/copy-file-path ()
    "Copy the current buffer's path to the kill ring."
    (interactive)
    (let ((file-name (buffer-file-name)))
      (if file-name
          (progn
            (kill-new file-name)
            (message "Copied buffer file name '%s' to the clipboard." file-name))
        (message "No file associated with this buffer."))))

  (defun cat/copy-file-name ()
    "Copy the current buffer's file name to the kill ring."
    (interactive)
    (let ((file-name (buffer-file-name)))
      (if file-name
          (progn
            (kill-new (file-name-nondirectory file-name))
            (message "Copied buffer file name '%s' to the clipboard." (file-name-nondirectory file-name)))
        (message "No file associated with this buffer."))))

  (defun cat/copy-file-name-base ()
    "Copy the current buffer's file name to the kill ring."
    (interactive)
    (let ((file-name (buffer-file-name)))
      (if file-name
          (progn
            (kill-new (file-name-base file-name))
            (message "Copied buffer file name '%s' to the clipboard." (file-name-base file-name)))
        (message "No file associated with this buffer."))))

  (defun cat/copy-directory-path ()
    "Copy the current buffer's directory path to the kill ring."
    (interactive)
    (let ((file-name (buffer-file-name)))
      (if file-name
          (progn
            (kill-new (file-name-directory file-name))
            (message "Copied buffer directory '%s' to the clipboard." (file-name-directory file-name)))
        (message "No file associated with this buffer."))))

  (defun split-window-below-and-focus ()
    "Split the window vertically and focus the new window."
    (interactive)
    (split-window-below)
    (windmove-down)
    (when (and (boundp 'golden-ratio-mode)
               (symbol-value golden-ratio-mode))
      (golden-ratio)))

  (defun split-window-right-and-focus ()
    "Split the window horizontally and focus the new window."
    (interactive)
    (split-window-right)
    (windmove-right)
    (when (and (boundp 'golden-ratio-mode)
               (symbol-value golden-ratio-mode))
      (golden-ratio)))
  )

;; Vim mode
(use-package evil
  :init
  (setq evil-want-C-u-scroll t)
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1)
  (evil-set-undo-system 'undo-redo)
  (setq-default evil-escape-delay 0.2))

(use-package evil-collection
  :after evil
  :custom (evil-collection-setup-minibuffer t)
  :config
  (evil-collection-init))

(use-package key-chord
  :config
  (key-chord-mode 1)
  (key-chord-define evil-insert-state-map "fd" 'evil-normal-state))


(use-package evil-collection
  :after evil
  :demand
  :init
  (setq evil-collection-magit-use-z-for-folds nil)
  :config
  (evil-collection-init))

(use-package evil-goggles
  :after evil
  :demand
  :init
  (setq evil-goggles-duration 0.05)
  :config
  (push '(evil-operator-eval
          :face evil-goggles-yank-face
          :switch evil-goggles-enable-yank
          :advice evil-goggles--generic-async-advice)
        evil-goggles--commands)
  (evil-goggles-mode)
  (evil-goggles-use-diff-faces))

(use-package evil-snipe
  :after evil
  :demand
  :config
  (evil-snipe-mode +1)
  (evil-snipe-override-mode +1)
  (add-hook 'magit-mode-hook 'turn-off-evil-snipe-override-mode))

(use-package evil-nerd-commenter
  :after general
  :general
  (general-nvmap
    "gc" 'evilnc-comment-operator
    "gC" 'evilnc-copy-and-comment-operator))

(use-package evil-surround
  :after general
  :general
  (:states 'operator
   "s" 'evil-surround-edit
   "S" 'evil-Surround-edit)
  (:states 'visual
   "s" 'evil-surround-region
   "gS" 'evil-Surround-region))

;; Which Key
(use-package which-key
  :init
  (setq which-key-separator " ")
  (setq which-key-prefix-prefix "+")
  :config
  (which-key-mode))

(use-package helm
  :straight t
  :config
  (helm-mode 1))

(use-package persistent-scratch
  :general
  (cat/leader-keys
    "bs" '((lambda ()
             "Load persistent-scratch if not already loaded"
             (interactive)
             (progn
               (unless (boundp 'persistent-scratch-mode)
                 (require 'persistent-scratch))
               (pop-to-buffer "*scratch*")))
           :wk "scratch"))
  :init
  (setq persistent-scratch-autosave-interval 60)
  :config
  (persistent-scratch-setup-default))

;; Project management
(use-package projectile
  :demand
  :init
  (setq projectile-completion-system 'helm)
  :general
  (cat/leader-keys
    :states 'normal
    "p" '(:keymap projectile-command-map :which-key "project")
    "p <escape>" 'keyboard-escape-quit
    "p b" 'projectile-switch-to-buffer
    "p a" '(projectile-add-known-project :wk "add known")
    "p f" 'projectile-find-file-dwim
    "p t" '(projectile-run-vterm :wk "term")
    "p s" 'helm-projectile-rg
    "p p" 'projectile-persp-switch-project
    )
  :config
  (projectile-mode))

(use-package helm-projectile
  :after projectile helm
  :config
  (helm-projectile-on))

;; (use-package helm-rg :after helm)

;; Workspaces
(use-package perspective
  :commands (persp-new persp-switch persp-state-save)
  :general
  (cat/leader-keys
    "l" '(:ignore true :wk "tab")
    "l l" 'persp-switch
    "l TAB" 'persp-switch-last
    "l x" 'persp-kill
    "l p" 'persp-prev
    "l n" 'persp-next
    "l d" '((lambda () (interactive) (persp-kill (persp-current-name))) :wk "kill current")
    "l D" '((lambda () (interactive) (persp-kill (persp-names))) :wk "kill all")
    "l m" '(cat/main-tab :wk "main"))
  :init
  (setq persp-suppress-no-prefix-key-warning t)
  (setq persp-state-default-file (expand-file-name ".persp" user-emacs-directory))
  (defun cat/main-tab ()
    "Jump to the dashboard buffer, if doesn't exists create one."
    (interactive)
    (persp-switch "main")
    (switch-to-buffer dashboard-buffer-name)
    (dashboard-mode)
    (dashboard-insert-startupify-lists)
    (dashboard-refresh-buffer))
  :config
  (persp-mode)
  (add-hook 'kill-emacs-hook #'persp-state-save))

(use-package persp-projectile
  :after projectile)

(use-package eyebrowse
  :after perspective
  :general
  (cat/leader-keys
    "l w" '(:ignore true :wk "Window layout")
    "l w d" 'eyebrowse-close-window-config
    "l w TAB" 'eyebrowse-last-window-config
    "l w w" 'eyebrowse-switch-to-window-config
    "l w 1" 'eyebrowse-switch-to-window-config-1
    "l w 2" 'eyebrowse-switch-to-window-config-2
    "l w 3" 'eyebrowse-switch-to-window-config-3
    "l w 4" 'eyebrowse-switch-to-window-config-4
    "l w 5" 'eyebrowse-switch-to-window-config-5
    "l w 6" 'eyebrowse-switch-to-window-config-6
    "l w 7" 'eyebrowse-switch-to-window-config-7
    "l w 8" 'eyebrowse-switch-to-window-config-8
    "l w 9" 'eyebrowse-switch-to-window-config-9
    "l w 0" 'eyebrowse-switch-to-window-config-0)
  :config
  (eyebrowse-mode t))

(use-package avy
  :general
  (general-def
    :states '(normal visual motion)
    :keymaps 'override

    ;; hack to support magit stages
    "s" (lambda ()
          (interactive)
          (if (eq major-mode 'magit-status-mode)
              (call-interactively 'magit-stage)
            (call-interactively 'evil-avy-goto-char-timer)))
    "gl" 'evil-avy-goto-line
    ";" 'avy-resume
    ))

(use-package so-long
  :config (global-so-long-mode 1))

;; hide minor modes
(use-package minions
  :custom (minions-prominent-modes '(flycheck-mode
                                     projectile-mode))
  :config (minions-mode 1))

(use-package deadgrep
  :commands (deadgrep--read-search-term) ;; used by deadgrep-extended
  :general
  (cat/leader-keys
    "p r" 'deadgrep))

(provide 'init-core)
