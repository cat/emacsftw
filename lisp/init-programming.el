;; Copilot
(use-package copilot
  :straight (:host github :repo "zerolfx/copilot.el" :files ("dist" "*.el"))
  :diminish
  :general
  (general-define-key
    :states '(insert)
    :keymaps 'override
    "TAB" 'cat/copilot-tab)
  :hook
  (prog-mode . (lambda ()
                "only set initial-major-mode after loading org"
                (copilot-mode 1)))
  :config
  (setq copilot-indent-offset-warning-disable t)
  
  (defun cat/no-copilot-mode ()
    (copilot-mode -1))

  (defvar cat/no-copilot-modes '(shell-mode
                                 inferior-python-mode
                                 eshell-mode
                                 term-mode
                                 vterm-mode
                                 comint-mode
                                 compilation-mode
                                 debugger-mode
                                 dired-mode-hook
                                 compilation-mode-hook
                                 flutter-mode-hook
                                 minibuffer-mode-hook)
    "Modes in which copilot is inconvenient.")

  (defun cat/copilot-disable-predicate ()
    "When copilot should not automatically show completions."
    (or (member major-mode cat/no-copilot-modes)))

  (add-to-list 'copilot-disable-predicates #'cat/copilot-disable-predicate)
  (global-copilot-mode 0) ;; wanted to 1, but infinite loop for some reason in the initial scratch buffer
  :init
  (defun cat/copilot-tab ()
    "Tab command that will complet with copilot if a completion is
available. Otherwise will try company, yasnippet or normal
tab-indent."
    (interactive)
    (or (copilot-accept-completion)
        (indent-for-tab-command)))
  )


;; GIT
(use-package magit
  :general
  (cat/leader-keys
    "g b" 'magit-blame
    "g s" 'magit-status
    "g g" 'magit-status
    "g G" 'magit-status-here
    "g L" 'magit-log)
  (general-nmap
    :keymaps '(magit-status-mode-map
               magit-stash-mode-map
               magit-revision-mode-map
               magit-process-mode-map
               magit-hunk-section-map
               magit-diff-mode-map)
    "TAB" #'magit-section-toggle
    "<escape>" #'transient-quit-one
    "s" #'magit-stage)
  (cat/local-leader-keys
    :keymaps 'with-editor-mode-map
    :states '(normal motion)
    "," #'with-editor-finish
    "c" #'with-editor-finish
    "a" #'with-editor-cancel
    "k" #'with-editor-cancel)
  (cat/local-leader-keys
    :states '(normal motion)
    :keymaps 'magit-log-select-mode-map
    "," #'magit-log-select-pick
    "c" #'magit-log-select-pick
    "a" #'magit-log-select-quit
    "k" #'magit-log-select-quit)
  :init
  (setq magit-display-buffer-function 'magit-display-buffer-traditional)
  (setq magit-log-arguments '("--graph" "--decorate" "--color"))
  (setq git-commit-fill-column 72)
  (setq magit-hunk-section-map (make-sparse-keymap))
  (define-key magit-hunk-section-map "s" 'magit-stage)
  :config
  (setq magit-buffer-name-format (concat "*" magit-buffer-name-format "*"))
  (evil-define-key* '(normal visual) magit-mode-map
    "zz" #'evil-scroll-line-to-center)
  )

;; more beautiful diffs
(use-package magit-delta
  :hook (magit-mode . magit-delta-mode))

(use-package git-link
  :general
  (cat/leader-keys
    "gl" '(:ignore t :which-key "Links")
    "gl l" 'git-link)
  :init
  (setq git-link-use-commit t))

;; General code stuff

(use-package flycheck
  :config (global-flycheck-mode 1))

(use-package flycheck-indicator
  :hook (flycheck-mode . flycheck-indicator-mode))

(use-package flyspell
  :ensure t
  :hook ((markdown-mode org-mode) . flyspell-mode)
  :custom (flyspell-use-meta-tab nil)
  (flyspell-auto-correct-binding (kbd "")))

(use-package treesit-auto
  :straight t
  :custom (treesit-auto-install t)
  :config (global-treesit-auto-mode 1))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :hook css-mode)

(use-package paren
  :custom ((show-paren-style 'parenthesis)
           (show-paren-delay 0))
  :config (show-paren-mode 1))

(use-package whitespace
  :hook ((prog-mode text-mode) . whitespace-mode)
  :custom (whitespace-line-column nil)
  (whitespace-style '(face trailing tabs missing-newline-at-eof empty indentation)))

;; RUBY

(use-package rubocop
  :hook ((ruby-mode enh-ruby-mode) . rubocop-mode))

;; Markdown

(use-package markdown-mode
  :hook (markdown-mode . auto-fill-mode)
  :custom (markdown-gfm-use-electric-backquote nil))

;; yaml

(use-package yaml-mode)

;; php

(use-package lsp-mode
  :ensure t)

(use-package php-mode
  :ensure t
  :config
  (lsp-mode t))


;; AucTeX

(use-package auctex
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (add-hook 'LaTeX-mode-hook 'visual-line-mode)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode)
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (setq reftex-plug-into-AUCTeX t)
  (setq TeX-PDF-mode t)

  ;; Use Skim as viewer, enable source <-> PDF sync
  ;; make latexmk available via C-c C-c
  ;; Note: SyncTeX is setup via ~/.latexmkrc (see below)
  (add-hook 'LaTeX-mode-hook (lambda ()
                               (push
                                '("latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
                                  :help "Run latexmk on file")
                                TeX-command-list)))
  (add-hook 'TeX-mode-hook #'(lambda () (setq TeX-command-default "latexmk")))

  ;; use Skim as default pdf viewer
  ;; Skim's displayline is used for forward search (from .tex to .pdf)
  ;; option -b highlights the current line; option -g opens Skim in the background  
  (setq TeX-view-program-selection '((output-pdf "PDF Viewer")))
  (setq TeX-view-program-list
        '(("PDF Viewer" "/Applications/Skim.app/Contents/SharedSupport/displayline -b -g %n %o %b")))
  )


(provide 'init-programming)
