;; Themes
(use-package doom-themes
  :config
  (load-theme 'doom-molokai t))

;; UI
(setq display-line-numbers-type 'relative)
(global-display-line-numbers-mode) 
(display-line-numbers-mode 1)

;; smooth scrolling
(pixel-scroll-precision-mode 1)
(setq scroll-preserve-screen-position t
      scroll-margin 5
      scroll-conservatively 101)

(use-package golden-ratio
  :demand
  :init
  (golden-ratio-mode 1))

(use-package all-the-icons :demand)
(use-package all-the-icons-completion
  :after (all-the-icons)
  :init
  (all-the-icons-completion-mode))

(use-package doom-modeline
  :demand
  :init
  (setq doom-modeline-buffer-encoding nil)
  (setq doom-modeline-env-enable-python nil)
  (setq doom-modeline-height 15)
  (setq doom-modeline-project-detection 'projectile)
  :config
  (doom-modeline-mode 1)
  (set-face-attribute 'doom-modeline-evil-insert-state nil :foreground "orange"))

(use-package dashboard
  :demand
  :init
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
  (setq dashboard-projects-backend 'projectile)
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title nil)
  (setq dashboard-set-footer nil)
  ;; (setq dashboard-startup-banner [VALUE])
  (setq dashboard-set-navigator t)
  (setq dashboard-navigator-buttons
        `((;; Github
           (,(all-the-icons-octicon "mark-github" :height 1.1 :v-adjust 0.0)
            "Github"
            "Go to github"
            (lambda (&rest _) (browse-url "https://github.com/")))
           ;; Codebase
           (,(all-the-icons-faicon "briefcase" :height 1.1 :v-adjust -0.1)
            "GitLab"
            "Go to GitLab"
            (lambda (&rest _) (browse-url "https://gitlab.com/")))
           ;; Perspectives
           (,(all-the-icons-octicon "history" :height 1.1 :v-adjust 0.0)
            "Restore"
            "Restore"
            (lambda (&rest _) (persp-state-load persp-state-default-file)))
           )))
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-items '((recents . 5)
                          (bookmarks . 5)
                          (projects . 5)
                          (registers . 5)))
  )

(provide 'init-ui)
